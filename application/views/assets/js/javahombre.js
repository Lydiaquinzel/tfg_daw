var params = window.location.search.substring(1).split("&"); // Recoge los datos'params' del formulario, separados por "&".
var nombre = params[0]; // Asignamos 'nombre' a 'params[0]'.
var apellidos = params[1]; // Asignamos 'apellidos' a 'params[1]'.
var fecha = params[2]; // Asignamos 'fecha' a 'params[2]'.
var fechaActual = new Date(); // Recogemos la fecha actual (SISTEMA)
var fechaActual = fechaActual.getDate() + "-" + (fechaActual.getMonth() + 1); // Cogemos el dia y el mes de la fecha actual (SISTEMA).

// MENSAJES PAGINA ELEGIDA:

window.alert('Has seleccionado: Hombre.'); // Ventana emergente: Sexo escogido.

// FELICITACION:

if (fechaActual == (fecha[0] + "/" + fecha[1])) { // Felicitación de cumpleaños.
    window.alert("¡Felicidades!"); // Ventana emergente: Felicitación de cumpleaños.
}

// COOKIES:

var todasLasGalletas = document.cookie; // Recogemos las Cookies (estan todas en la misma variable).
var cookieArray = document.cookie.split(';'); // Las guardamos en un array ya separadas.
var nombreCookie = ["nombre=", "apellidos=", "dni=","sexo=", "tiempo="]; // Array que guarda el 'nombre' de nuestras cookies.
var valorCookie = []; // Array que guarda el 'valor' de nuestras cookies.


    if (cookieArray[0] != "") {
        // COOKIES:
        for (let i = 0; i < cookieArray.length; i++) {
            let miCookie = cookieArray[i];

            while (miCookie.charAt(0) == ' ') {
                miCookie = miCookie.substring(1);
            }
            if (miCookie.indexOf(nombreCookie[i]) == 0) {
                valorCookie[i] = miCookie.substring(nombreCookie[i].length, miCookie.length);
            }
    }
        // MENSAJE BIENVENIDA:
        alert("Bienvenido " + valorCookie[0] + " " + valorCookie[1]);
        alert("Usted ha pasado " + valorCookie[4] + " en la página.");
    } else {
        // LOCALSTORAGE
        nombre = sessionStorage.getItem("nombre");
        apellidos = sessionStorage.getItem("apellidos");
        dni = sessionStorage.getItem("dni");
        sexo = sessionStorage.getItem("sexo");
        // MENSAJE BIENVENIDA:
        alert("Bienvenido " + nombre + " " + apellidos);
    }