<script>
    <head>
            
        <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/sweetalert2@11.1.10/dist/sweetalert2.css"></link>
        <link type="text/css" src="../library/bootstrap-3.3.7-dist/css/bootstrap.min.css"></link>

    </head>
    <body>

        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11.1.10/dist/sweetalert2.all.min.js"></script>
        <script src="../library/bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
        <script src="../library/jquery-3.6.0.js"></script>

    </body>
</script>