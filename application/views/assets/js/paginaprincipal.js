function enviar() {

   // Recoge los datos mediante la id:
   var nombres = document.getElementById("nombre").value;
   var apellidos = document.getElementById("apellidos").value; 
   var sexo = document.getElementById("sexoElegido").value;
   var dni = document.getElementById("dni").value;
   var foto = document.getElementById("foto").value;

   var fecha = document.getElementById("nacimiento").value.split("/"); // Recoge los datos mediante la id (introducidos con "/").
   var fechaActual = new Date(); // Recogemos la fecha actual.

   // Introducimos fecha actual y después hacemos el split:
   fechaActual = (fechaActual.getDate() + "/" + (fechaActual.getMonth() + 1) + "/" + fechaActual.getFullYear()).split("/");

   if (!nombres || !apellidos || !fecha || !dni || !foto) {  // if para comprobar campos vacíos del formulario.

      Swal.fire( { // Ventana emergente: Faltan datos por rellenar.
         title: 'OOOPS...',
         imageUrl: '../Imagenes/arielOops.gif',
         imageWidth: 400,
         imageHeight: 220,
         confirmButtonText: 'Volver',
         text: 'Faltan datos por rellenar.'
      });

   } else { // Si los campos están rellenos se ejecuta el else.
      //alert("dia: " + fecha[0] + "mes: " + fecha[1] + "año: " + fecha[2]);

      // Comprobamos el año:
      if (parseInt(fechaActual[2] - 18) < parseInt(fecha[2])) {
         Swal.fire( { // Ventana emergente: Debes ser mayor de edad.
            icon: 'error',
            title: 'Lo sentimos',
            text: 'Debes ser mayor de edad para ver el contenido.',
          });
      } else {
         // Si se trata del mismo año, comparamos los meses:
         if(parseInt(fechaActual[2] - 18) == parseInt(fecha[2]) && parseInt(fecha[1]) > parseInt(fechaActual[1]) ) {
            Swal.fire( { // Ventana emergente: Debes ser mayor de edad.
               icon: 'error',
               title: 'Lo sentimos',
               text: 'Debes ser mayor de edad para ver el contenido.',
             });
         } else {
            // Si se trata del mismo año y mes, comparamos los días:
            if(parseInt(fechaActual[2] - 18) == parseInt(fecha[2]) && 
               parseInt(fecha[1]) == parseInt(fechaActual[1]) &&
               parseInt(fecha[0]) > parseInt(fechaActual[0])) {
               Swal.fire( { // Ventana emergente: Debes ser mayor de edad.
                  icon: 'error',
                  title: 'Lo sentimos',
                  text: 'Debes ser mayor de edad para ver el contenido.',
               });
            } else {
               // Comprobamos que el dni tenga un formato adecuado:
               if ( /^[0-9]{8,8}[A-Za-z]$/.test(dni) ) { // Expresion regular para obtener un dato formato DNI (11111111A).

                  if (/^([A-ZÁÉÍÓÚ]{1}[a-zñáéíóú]+[\s]*)+$/.test(nombres)) { // Expresion regular para obtener un nombre en formato valido.

                     if (/^([A-ZÁÉÍÓÚ]{1}[a-zñáéíóú]+[\s]*)+$/.test(apellidos)) { // Expresion regular para obtener un apellido/apellidos en formato valido.

                           // Validacion tamaño del archivo (foto):
                           var imgsize = document.getElementById("foto"); 
                           var file = imgsize.files[0];
                              if (file.size > 2097152) {
                                 Swal.fire( { // Ventana emergente: El archivo no puede superar los 2Mb.
                                    title: 'Archivo demasiado grande',
                                    imageUrl: '../Imagenes/tefiti.gif',
                                    imageWidth: 220,
                                    imageHeight: 280,
                                    confirmButtonText: 'Volver',
                                    text: 'El archivo no puede superar los 2Mb.',
                                 });

                              } else { // Si el archivo NO supera los 2Mb...

                                 // Guardamos los datos en las COOKIES o en LOCALSTORAGE:                 
                                 cookies();

                                 // if según el sexo escogido. Cada opción lleva a una página diferente y envía los datos.
                                 if (sexo == "Hombre") {
                                    window.open("../Paginas/paginahombre.html?" + nombres + "&" + apellidos + "&" + document.getElementById("nacimiento").value + "&" + sexo); // Ponemos la ruta terminada en ? para que no concatene el enlace con el resto de valores.
                                 }                                                                                                       // También pasamos los datos del formulario a la página destino.
                                 if (sexo == "Mujer") {
                                    window.open("../Paginas/paginamujer.html?" + nombres + "&" + apellidos + "&" + document.getElementById("nacimiento").value + "&" + sexo);
                                 }
                                 if (sexo == "Sin especificar") {
                                    window.open("../Paginas/paginasinespecificar.html?" + nombres + "&" + apellidos + "&" + document.getElementById("nacimiento").value + "&" + sexo);
                                 }
                        } // cierre else 'archivo valido'.

                     } else {
                        Swal.fire( { // Ventana emergente: El apellido no es válido.
                           icon: 'error',
                           title: 'ERROR',
                           text: 'El apellido no es válido.',
                        });
                     } // cierre else 'comprobacion apellido'.

                  } else {
                     Swal.fire( { // Ventana emergente: El nombre no es válido.
                        icon: 'error',
                        title: 'ERROR',
                        text: 'El nombre no es válido.',
                     });
                  } // cierre else 'comprobacion nombre'.

                  } else {
                  Swal.fire({ // Ventana emergente: Faltan datos por rellenar.
                     title: 'DNI incorrecto',
                     imageUrl: '../Imagenes/moana.gif',
                     imageWidth: 290,
                     imageHeight: 330,
                     confirmButtonText: 'Volver',
                     text: 'Parece que el DNI no es válido.'
                  });
               } // cierre else 'mensaje DNI incorrecto'.
            } // cierre else 'comprobación DNI'.
         } // cierre else 'comprobacion fecha nacimiento -> dia'.
      } // cierre else 'comprobacion fecha nacimiento -> mes'.
   } // cierre else 'comprobacion fecha nacimiento -> año'.
} // cierre boton 'enviar()'.

//__________________________________________________________________________________________________________________________________________
// GUARDAR COOKIES | LOCALSTORAGE
// Funcion 'cookies()' -> Se ejecuta cuando se checkea la casilla de las cookies (formulario).
//                                      - Guarda los datos en las cookies si está marcada.
//                                      - Guarda los datos en localstorage si NO está marcada. 

function cookies() { 

   if (document.getElementById("checkboxCookies").checked) {
      // Cookies
      document.cookie = "nombre=" + nombre.value + ";"; // nombre.value -> nombre es el nombre del input (HTML) (asi en todos los casos)
      document.cookie = "apellidos=" + apellidos.value + ";";
      document.cookie = "dni=" + dni.value + ";";
      document.cookie = "sexo=" + sexoElegido.value + ";";
      document.cookie = "tiempo="+ horas +";";
   } else {
       // Storage
       sessionStorage.setItem("nombre", nombre.value);
       sessionStorage.setItem("apellidos", apellidos.value);
       sessionStorage.setItem("dni", dni.value);
       sessionStorage.setItem("sexo", sexoElegido.value);
       sessionStorage.setItem("tiempo", horas);
       
   }
} // cierre funcion'cookies()'.

//__________________________________________________________________________________________________________________________________________
// GUARDAR COOKIE - NOMBRE.
// Funcion 'getCookieNombre()' -> Guarda la cookie 'nombre'.

function getCookieNombre() { 
   let name = "nombre=";
   let Cookiedecodificada = decodeURIComponent(document.cookie);
   let cookieArray = Cookiedecodificada.split(';');

   for (let i = 0; i < cookieArray.length; i++) {
     let miCookie = cookieArray[i];
     while (miCookie.charAt(0) == ' ') {
       miCookie = miCookie.substring(1);
     }
     if (miCookie.indexOf(name) == 0) {
       return miCookie.substring(name.length, miCookie.length);
     }
   }
   return "";
 } // cierre funcion'getCookieNombre()'.

//__________________________________________________________________________________________________________________________________________
// GUARDAR COOKIE - TIEMPO.
// Funcion 'getCookieTiempo()' -> Guarda la cookie 'tiempo'.

function getCookieTiempo() {
   let name = "tiempo=";
   let Cookiedecodificada = decodeURIComponent(document.cookie);
   let cookieArray = Cookiedecodificada.split(';');

   for (let i = 0; i < cookieArray.length; i++) {
     let miCookie = cookieArray[i];
     while (miCookie.charAt(0) == ' ') {
       miCookie = miCookie.substring(1);
     }
     if (miCookie.indexOf(name) == 0) {
       return miCookie.substring(name.length, miCookie.length);
     }
   }
   return "";
 } // cierre funcion'getCookieTiempo()'.

//__________________________________________________________________________________________________________________________________________
// VERIFICAR COOKIES.
/*
function verificarCookies() {               
   var usuario = getCookieNombre();
   var tiempoTranscurrido = getCookieTiempo();
   if (usuario == "") {
     return false;
   } else if (usuario != "") {
     alert("Bienvenido de vuelta " + usuario + " Su sesión anterior fue de "+ tiempoTranscurrido);
   }
 } // cierre funcion'verificarCookies()'.
*/
//__________________________________________________________________________________________________________________________________________
// CRONOMETRO PAGINA.
// Se inicia por defecto ('window.onload').

window.onload = init();
min = 0, hrs = 0;
var cronos, horas;

function init() {
    cronos = setInterval(function timer(){
    sec = parseInt(document.getElementById('time').value);
    document.getElementById('time').value = eval(sec + 1);

    if (sec > 60) {
        sec = document.getElementById('time').value = 0;
        min++;

        if (min > 60) {
            min = 0;
            hrs++;
        }
    }

    horas = document.getElementById("hora").innerHTML = hrs + " horas " + min + " minutos " + sec + " segundos ";
    }, 1000);
}
//__________________________________________________________________________________________________________________________________________
