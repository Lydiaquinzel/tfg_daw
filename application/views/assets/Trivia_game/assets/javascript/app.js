
$(document).ready(function () {
  // button and click controls
  // $('#remaining-time').hide()
  document.querySelector('#remaining-time').style.display = 'none'
  $('#start').on('click', trivia.startGame)
  $(document).on('click', '.option', trivia.guessChecker)
})

// holds our trivia variable and its various properties
var trivia = {
  correct: 0,
  incorrect: 0,
  unanswered: 0,
  currentSet: 0,
  timer: 20,
  timerOn: false,
  timerId: '',
  // question and answer array
  questions: {
    q1: '¿Cuál de estas películas pertenece al periodo conocido como "El Renacimiento de Disney"?',
    q2: '¿Cuál de estas películas no está producida por Disney?',
    q3: '¿Qué personaje no es una princesa Disney?',
    q4: '¿Qué película de animación no ha sido todavía adaptada a película de acción en vivo?',
    q5: '¿Cúal el el nombre del compañero animal de Moana?',
    q6: '¿En qué película aparece la canción "La estrella azul"?',
    q7: '¿Qué película de Disney Pixar no ha ganado el Oscar a la mejor película de animación?'
  },
  options: {
    q1: ['Blancanieves y los siete enanitos', 'Frozen', 'La sirenita', 'El libro de la selva'],
    q2: ['Star Wars: El último Jedi', 'Tron: Legacy', 'Vengadores: Endgame', 'Como entrenar a tu dragón'],
    q3: ['Rapunzel', 'Tiana', 'Vanellope', 'Merida'],
    q4: ['Hercules', 'La bella y la bestia', 'Cenicienta', 'Dumbo'],
    q5: ['Iago', 'Scrump', 'Pua', 'Tamatoa'],
    q6: ['Pinocho', 'Peter Pan', 'Robin Hood', 'Alicia en el país de las maravillas'],
    q7: ['Coco', 'Brave', 'Incredibles 2', 'Finding Nemo']
  },
  answers: {
    q1: 'La sirenita',
    q2: 'Como entrenar a tu dragón',
    q3: 'Vanellope',
    q4: 'Hercules',
    q5: 'Pua',
    q6: 'Pinocho',
    q7: 'Incredibles 2'
  },

  // function to start the game
  startGame: function () {
    trivia.currentSet = 0
    trivia.correct = 0
    trivia.incorrect = 0
    trivia.unanswered = 0
    clearInterval(trivia.timerId)

    // display
    $('#game').show()

    //  display results
    $('#results').html('')

    $('#timer').text(trivia.timer)

    // hide button
    $('#start').hide()

    $('#remaining-time').show()

    trivia.nextQuestion()
  },

  // function for going through our questions
  nextQuestion: function () {
    trivia.timer = 12
    $('#timer').removeClass('last-seconds')
    $('#timer').text(trivia.timer)

    if (!trivia.timerOn) {
      trivia.timerId = setInterval(trivia.timerRunning, 1000)
    }

    var questionContent = Object.values(trivia.questions)[trivia.currentSet]
    $('#question').text(questionContent)

    // array
    var questionOptions = Object.values(trivia.options)[trivia.currentSet]

    // displays the options
    $.each(questionOptions, function (index, key) {
      $('#options').append($('<button class="option btn btn-info btn-lg">' + key + '</button>'))
    })
  },
  // how we decrement timer and record non-answers
  timerRunning: function () {
    if (trivia.timer > -1 && trivia.currentSet < Object.keys(trivia.questions).length) {
      $('#timer').text(trivia.timer)
      trivia.timer--
      if (trivia.timer === 4) {
        $('#timer').addClass('last-seconds')
      }
    }
    // if timer reaches 0, add to incorrect total
    else if (trivia.timer === -1) {
      trivia.unanswered++
      trivia.result = false
      clearInterval(trivia.timerId)
      resultId = setTimeout(trivia.guessResult, 1000)
      $('#results').html('<h3>¡Fuera de tiempo! La respuesta es: ' + Object.values(trivia.answers)[trivia.currentSet] + '</h3>')
    }
    // if all the questions have been shown end the game, show results
    else if (trivia.currentSet === Object.keys(trivia.questions).length) {
      // appends results to page
      $('#results')
        .html('<h3>¡Gracias por jugar!</h3>' +
          '<p>¡Correcto!: ' + trivia.correct + '</p>' +
          '<p>Incorrecto: ' + trivia.incorrect + '</p>' +
          '<p>Sin respuesta: ' + trivia.unanswered + '</p>' +
          '<p>¡Juega otra vez!</p>')

      // hide
      $('#game').hide()

      // show button again to start new game
      $('#start').show()
    }
  },

  guessChecker: function () {
    // timer ID for gameResult setTimeout/
    var resultId

    // current question and answer
    var currentAnswer = Object.values(trivia.answers)[trivia.currentSet]

    // if the text of the option picked matches the answer of the current question, increment correct
    if ($(this).text() === currentAnswer) {
      // Cambio el color a verde si la respuesta es incorrecta:
      $(this).addClass('btn-success').removeClass('btn-info')

      trivia.correct++
      clearInterval(trivia.timerId)
      resultId = setTimeout(trivia.guessResult, 1000)
      $('#results').html('<h3>¡Respuesta correcta!</h3>')
    } else {
      // Cambio el color a rojo si la respuesta es incorrecta:
      $(this).addClass('btn-danger').removeClass('btn-info')

      trivia.incorrect++
      clearInterval(trivia.timerId)
      resultId = setTimeout(trivia.guessResult, 1000)
      $('#results').html('<h3>¡Respuesta incorrecta! La respuesta es: ' + currentAnswer + '</h3>')
    }
  },
  // removes previous results
  guessResult: function () {
    // takes us to next question
    trivia.currentSet++

    // remove the options and results
    $('.option').remove()
    $('#results h3').remove()

    // begin next question
    trivia.nextQuestion()
  }

}
